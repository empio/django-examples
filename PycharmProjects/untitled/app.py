# full_name = 'John Smith'
# age = 20
# is_new = True

# name = input('What is your name? ')
# favorite_color = input('What is your favorite color? ')
# print(name + ' likes ' + favorite_color)

# weight_lbs = input('Weight (lbs): ')
# weight_kg = int(weight_lbs) * 0.45
# print(weight_kg)

# name = 'Jennifer'
# print(name[1:-1])

# x = (2 + 3) * 10 - 3
# print(x)

# price = 1000000
# has_good_credit = True
# if has_good_credit:
#     down_payment = 0.1 * price
# else:
#     down_payment = 0.2 * price
# print(f"Down payment: ${down_payment}")

# name = "John Smith"
# if len(name) < 3:
#     print("Name must be at least 3 characters.")
# elif len(name) > 50:
#     print("Name must be a maximum of 50 characters.")
# else:
#     print("Name looks good!")

# weight = int(input('Weight: '))
# unit = input('(L)bs or (K)g: ')
# if unit.upper() == "L":
#     converted = weight * 0.45
#     print(f"You are {converted} kilos")
# else:
#     converted = weight / 0.45
#     print(f"You are {converted} pounds")

# secret_number = 9
# guess_count = 0
# guess_limit = 3
# while guess_count < guess_limit:
#     guess = int(input('Guess: '))
#     guess_count += 1
#     if guess == secret_number:
#         print('You won!')
#         break
# else:
#     print('Sorry, you failed!')

# command = ""
# started = False
# while True:  # command != "quit"
#     command = input("> ").lower()
#     if command == "start":
#         # print("Car started...")
#         if started:
#             print("Car is already started!")
#         else:
#             started = True
#             print("Car started...")
#     elif command == "stop":
#         # print("Car stopped.")
#         if not started:
#             print("Car is already stopped!")
#         else:
#             started = False
#             print("Car stopped.")
#     elif command == "help":
#         print("""
# car - to start the car
# stop - to stop the car
# quit - to quit
#         """)
#     elif command == "quit":
#         break
#     else:
#         print("Sorry, I don't understand that!")

# prices = [10, 20, 30]
# total = 0
# for price in prices:
#     total += price
# print(f"Total: {price}")

# numbers = [5, 2, 5, 2, 2]
# for x_count in numbers:
#     print('x' * x_count)

# numbers = [5, 2, 5, 2, 2]
# for x_count in numbers:
#     output = ''
#     for count in range(x_count):
#         output += 'x'
#     print(output)

# numbers = [3, 6, 2, 8, 4, 10]
# max = numbers[0]
# for number in numbers:
#     if number > max:
#         max = number
# print(max)

# matrix = [
#     [1, 3, 3],
#     [4, 5, 6],
#     [7, 8, 9]
# ]
# for row in matrix:
#     for item in row:
#         print(item)

# numbers = [2, 2, 4, 6, 3, 4, 6, 1]
# uniques = []
# for number in numbers:
#     if number not in uniques:
#         uniques.append(number)
# print(uniques)

# coordinates = (1, 2, 3)
# coordinates[0] * coordinates[1] * coordinates[2]
# x = coordinates[0]
# y = coordinates[1]
# z = coordinates[2]
# x * y * z
# x, y, z = coordinates

# customer = {
#     "name": "John Smith",
#     "age": 30,
#     "is_verified": True
# }
# print(customer.get("birthdate", "Jan 1 1980"))

# phone = input("Phone: ")
# digits_mapping = {
#     "1": "One",
#     "2": "Two",
#     "3": "Three",
#     "4": "Four"
# }
# output = ""
# for ch in phone:
#     output += digits_mapping.get(ch, "!") + " "
# print(output)

# message = input(">")
# words = message.split(' ')
# emojis = {
#     ":)": ":-)",
#     ":(": ":-("
# }
# output = ""
# for word in words:
#     output += emojis.get(word, word) + " "
# print(output)

# def emoji_converter(message):
#     words = message.split(" ")
#     emojis = {
#         ":)": ":-)",
#         ":(": ":-("
#     }
#     output = ""
#     for word in words:
#         output += emojis.get(word, word) + " "
#     return output
#
#
# message = input(">")
# # result = emoji_converter((message))
# # print(result)
# print(emoji_converter((message)))

# try:
#     age = int(input('Age: '))
#     income = 20000
#     risk = income / 0
#     print(age)
# except ZeroDivisionError:
#     print('Age cannot be 0.')
# except ValueError:
#     print('Invalid value')

# # Calculates and returns the square of a number
# def square(number):
#     return number * number

# class Point:
#     def move(self):
#         print("move")
#
#     def draw(self):
#         print("draw")
#
#
# point1 = Point()
# point1.x = 10
# point1.y = 20
# print(point1.x)
# point1.draw()
#
# point2 = Point()
# point2.x = 1
# print(point2.x)


# class Point:
#     def __init__(self, x, y):
#         self.x = x
#         self.y = y
#
#     def move(self):
#         print("move")
#
#     def draw(self):
#         print("draw")
#
#
# point = Point(10,20)
# point.x = 11
# print(point.x)


# class Person:
#     def __init__(self, name):
#         self.name = name
#
#     def talk(self):
#         print(f"Hi, I am {self.name}")
#
#
# john = Person("John Smith")
# john.talk()
#
# bob = Person("Bob Smith")
# bob.talk()


# class Mammal:
#     def walk(self):
#         print("walk")
#
#
# class Dog(Mammal):
#     # pass
#     def bark(self):
#         print("bark")
#
#
# class Cat(Mammal):
#     def be_annoying(self):
#         print("annoying")
#
#
# dog1 = Dog()
# dog1.walk()
#
# cat1 = Cat()
# cat1.be_annoying()

# import converters
# from converters import kg_to_lbs
#
# print(kg_to_lbs(100))
#
# print(converters.kg_to_lbs(70))

# from utils import find_max
#
# numbers = [10, 3, 6, 2]
# maximum = find_max(numbers)
# # print(max(numbers))
# print(maximum)

# import ecommerce.shipping
# from ecommerce.shipping import calc_shipping
# from ecommerce import shipping
#
# ecommerce.shipping.calc_shipping()
#
# calc_shipping()
#
# shipping.calc_shipping()

# import random
#
# for i in range(3):
#     print(random.random())
#     print(random.randint(10, 20))
#
# members = ['John', 'Marry', 'Bob', 'Mosh']
# leader = random.choice(members)
# print(leader)

# import random
#
#
# class Dice:
#     def roll(self):
#         first = random.randint(1, 6)
#         second = random.randint(1, 6)
#         return first, second
#
#
# dice = Dice()
# print(dice.roll())

from pathlib import Path

#path = Path("emails")
#print(path.exists())
#print(path.mkdir())
#print(path.rmdir())
path = Path()
#print(path.glob('*.py'))
for file in path.glob('*.py'):
    print(file)

# import openpyxl as xl
# from openpyxl.chart import BarChart, Reference
#
#
# def process_workbook(filename):
#     wb = xl.load_workbook(filename)
#     sheet = wb['Sheet1']
#     cell = sheet['a1']
#     cell = sheet.cell(1, 1)
#
#     for row in range(2, sheet.max_row + 1):
#         cell = sheet.cell(row, 3)
#         corrected_price = cell.value * 0.9
#         corrected_price_cell = sheet.cell(row, 4)
#         corrected_price_cell.value = corrected_price
#
#     values = Reference(sheet,
#                        min_row=2,
#                        max_row=sheet.max_row,
#                        min_col=4,
#                        max_col=4)
#
#     chart = BarChart()
#     chart.add_data(values)
#     sheet.add_chart(chart, 'e2')
#
#     wb.save(filename)
